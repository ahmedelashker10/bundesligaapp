//
//  TeamsViewController.h
//  BundesligaApp
//
//  Created by Ahmed Elashker on 6/26/15.
//  Copyright (c) 2015 Ahmed Elashker. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
#import "RequestManager.h"
#import "Team.h"
#import "TeamViewController.h"
#import "ViewManager.h"

@interface TeamsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSURLConnectionDelegate>

+ (TeamsViewController*)sharedInstance;

@property (strong, nonatomic) UITableView *teamsTableView;

@property (strong, nonatomic) IBOutlet UITableViewCell *cell;

@property (strong, nonatomic) TeamViewController *tvc;

@end