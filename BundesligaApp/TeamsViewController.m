//
//  TeamsViewController.m
//  BundesligaApp
//
//  Created by Ahmed Elashker on 6/26/15.
//  Copyright (c) 2015 Ahmed Elashker. All rights reserved.
//

#import "TeamsViewController.h"

@interface TeamsViewController ()

@property (nonatomic, strong) NSMutableData *activeDownload;
@property (nonatomic, strong) NSURLConnection *imageConnection;

@end

@implementation TeamsViewController

@synthesize activeDownload, imageConnection;
@synthesize teamsTableView, tvc;

+ (TeamsViewController*)sharedInstance
{
    static TeamsViewController* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

// custom init
- (id)init
{
    if (self = [super init])
    {
        self.title = @"Bundesliga";
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable) name:@"TeamsResolved" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushTeamVC) name:@"FixturesResolved" object:nil];
    }
    return self;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self setTableFrame];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initSubViews];
    
    if (![[DataManager sharedInstance] dataExists]) {
        [[ViewManager sharedInstance] showVL:self.view];
        [[RequestManager sharedInstance] requestTeams];
    }
    else
        [self refreshTable];
}

- (void)initSubViews
{
    teamsTableView = [[UITableView alloc] init];
    teamsTableView.dataSource = self;
    teamsTableView.delegate = self;
    
    [self setTableFrame];
    [self.view addSubview:teamsTableView];
}

- (void)setTableFrame
{
    CGRect tableFrame = self.view.frame;
    tableFrame.size.height -= self.navigationController.navigationBar.frame.size.height;
    tableFrame.origin.y = self.navigationController.navigationBar.frame.size.height;
    teamsTableView.frame = tableFrame;
    
    [[ViewManager sharedInstance] adjustToThisCenter:self.view.center];
}

- (void)requestData
{
    [[RequestManager sharedInstance] requestTeams];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 69;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [DataManager sharedInstance].teams.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell %ld", (long)indexPath.row];
    
    _cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (_cell == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"TeamCell" owner:self options:nil];
    }
    
    UIImageView *cellImage = (UIImageView *)[_cell viewWithTag:1];
    UILabel *cellText = (UILabel *)[_cell viewWithTag:2];
    
    Team *team = [[DataManager sharedInstance].teams objectAtIndex:indexPath.row];
    
    // Image retrieving
    NSString *encodedStr = [team.crestUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:encodedStr];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            
            // imagwWithData Always returning nil
            // SVG format not supported (check below link)
            // https://developer.apple.com/library/ios/documentation/UIKit/Reference/UIImage_Class/index.html
            UIImage *img = [UIImage imageWithData: imageData];
            if(img != nil)
            {
                cellImage.image = img;
            }
        });
    });
    
    if (cellImage.image == nil)
    {
        cellImage.image = [UIImage imageNamed:@"no-available.jpg"];
    }
    
    cellText.text = team.name;
    
    return _cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Fetch team object
    Team *team = [[DataManager sharedInstance].teams objectAtIndex:indexPath.row];
    
    // Init new team view controller
    tvc = [[TeamViewController alloc] init];
    tvc.title = team.name;
    
    // If key = value in teams fixtures dictionary, then request fixtures
    if ([[DataManager sharedInstance].teamsDic objectForKey:team.code] == team.code)
    {
        [[ViewManager sharedInstance] showVL:self.view];
        [[RequestManager sharedInstance] requestFixtures:team];
    }
    else
        [self pushTeamVC];
}

- (void)refreshTable
{
    // First part where team data surely exists
    // Set teams fixtures dic to default value (team codes for both K/V)
    [DataManager sharedInstance].teamsDic = [[NSMutableDictionary alloc] initWithObjects:[DataManager sharedInstance].teamCodes forKeys:[DataManager sharedInstance].teamCodes];
    
    [[ViewManager sharedInstance] hideVL];
    [teamsTableView reloadData];
}

- (void)pushTeamVC
{
    // Push the view controller.
    [[ViewManager sharedInstance] hideVL];
    [self.navigationController pushViewController:tvc animated:YES];
}

@end
