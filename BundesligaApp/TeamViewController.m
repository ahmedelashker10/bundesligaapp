//
//  TeamViewController.m
//  BundesligaApp
//
//  Created by Ahmed Elashker on 6/27/15.
//  Copyright (c) 2015 Ahmed Elashker. All rights reserved.
//

#import "TeamViewController.h"

@interface TeamViewController ()

@end

@implementation TeamViewController

@synthesize fixturesTableView, fixtures;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self initSubViews];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self setTableFrame];
}

- (void)initSubViews
{
    fixturesTableView = [[UITableView alloc] init];
    fixturesTableView.dataSource = self;
    fixturesTableView.delegate = self;
    
    [self setTableFrame];
    [self.view addSubview:fixturesTableView];
}

- (void)setTableFrame
{
    CGRect tableFrame = self.view.frame;
    tableFrame.size.height -= self.navigationController.navigationBar.frame.size.height;
    tableFrame.origin.y = self.navigationController.navigationBar.frame.size.height;
    fixturesTableView.frame = tableFrame;
    
    [fixturesTableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 69;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    NSString *teamCode;
    
    for (Team* team in [DataManager sharedInstance].teams) {
        if (team.name == self.title) {
            teamCode = team.code;
            break;
        }
    }
    
    if ([[[DataManager sharedInstance].teamsDic objectForKey:teamCode] isKindOfClass:[NSString class]]) {
        return 0;
    }
    else
    {
        fixtures = [[DataManager sharedInstance].teamsDic objectForKey:teamCode];
        
        return fixtures.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell %ld", (long)indexPath.row];
    
    _cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (_cell == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"FixtureCell" owner:self options:nil];
    }
    
    UIImageView *cellImage = (UIImageView *)[_cell viewWithTag:1];
    UILabel *cellText = (UILabel *)[_cell viewWithTag:2];
    UIImageView *cellImage2 = (UIImageView *)[_cell viewWithTag:3];
    
    // Adjust cell frame
    CGRect cellFrame = _cell.frame;
    cellFrame.size.width = tableView.frame.size.width;
    float widthDifference = cellFrame.size.width - _cell.frame.size.width;
    _cell.frame = cellFrame;
    
    // Label frame
    CGRect lblFrame = cellText.frame;
    lblFrame.size.width += widthDifference;
    cellText.frame = lblFrame;
    
    // Adjust away imgView
    CGRect awayFrame = cellImage2.frame;
    awayFrame.origin.x += widthDifference;
    cellImage2.frame = awayFrame;
    
    if (![fixtures isKindOfClass:[NSString class]]) {
        
        Fixture *fix = [fixtures objectAtIndex:indexPath.row];
        
        if (cellImage.image == nil)
        {
            cellImage.image = [UIImage imageNamed:@"no-available.jpg"];
        }
        if (cellImage2.image == nil)
        {
            cellImage2.image = [UIImage imageNamed:@"no-available.jpg"];
        }
        
        cellText.text = [NSString stringWithFormat:@"%@:%@", fix.home, fix.away];
    }
    
    return _cell;
}

@end
