//
//  Team.h
//  BundesligaApp
//
//  Created by Ahmed Elashker on 6/27/15.
//  Copyright (c) 2015 Ahmed Elashker. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Team : NSObject

@property (strong, nonatomic) NSString *code;

@property (strong, nonatomic) NSString *crestUrl;

@property (strong, nonatomic) NSString *name;

@property (strong, nonatomic) NSString *shortName;

@property (strong, nonatomic) NSString *squadMarketValue;

@property (strong, nonatomic) NSString *fixturesUrl;

@property (strong, nonatomic) NSString *playersUrl;

@property (strong, nonatomic) NSString *url;

@end
