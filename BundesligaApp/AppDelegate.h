//
//  AppDelegate.h
//  BundesligaApp
//
//  Created by Ahmed Elashker on 6/26/15.
//  Copyright (c) 2015 Ahmed Elashker. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
#import "ViewManager.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

