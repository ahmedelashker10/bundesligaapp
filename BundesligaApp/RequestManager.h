//
//  RequestManager.h
//  BundesligaApp
//
//  Created by Ahmed Elashker on 6/26/15.
//  Copyright (c) 2015 Ahmed Elashker. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataManager.h"

@interface RequestManager : NSObject

+ (RequestManager*)sharedInstance;

- (void)requestTeams;

- (void)requestFixtures:(Team*)team;

@end
