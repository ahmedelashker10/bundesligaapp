//
//  TeamViewController.h
//  BundesligaApp
//
//  Created by Ahmed Elashker on 6/27/15.
//  Copyright (c) 2015 Ahmed Elashker. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"

@interface TeamViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UITableView *fixturesTableView;

@property (strong, nonatomic) NSMutableArray *fixtures;

@property (strong, nonatomic) IBOutlet UITableViewCell *cell;

@end
