//
//  DataManager.h
//  BundesligaApp
//
//  Created by Ahmed Elashker on 6/26/15.
//  Copyright (c) 2015 Ahmed Elashker. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Fixture.h"
#import "Team.h"

@interface DataManager : NSObject

+ (DataManager*)sharedInstance;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@property (strong, nonatomic) NSMutableArray *teams;
@property (strong, nonatomic) NSMutableArray *teamCodes;
@property (strong, nonatomic) NSMutableDictionary *teamsDic;

- (BOOL)dataExists;

@end
