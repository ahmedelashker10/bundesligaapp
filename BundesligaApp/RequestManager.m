//
//  RequestManager.m
//  BundesligaApp
//
//  Created by Ahmed Elashker on 6/26/15.
//  Copyright (c) 2015 Ahmed Elashker. All rights reserved.
//

#import "RequestManager.h"

@implementation RequestManager

NSManagedObjectContext *context;

+ (RequestManager*)sharedInstance
{
    static RequestManager* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    if (self = [super init])
    {
        context = [[DataManager sharedInstance] managedObjectContext];
    }
    return self;
}

- (void)requestTeams
{
    NSString *urlString = @"http://api.football-data.org/alpha/soccerseasons/351/teams";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             //NSLog(@"Error,%@", [error localizedDescription]);
         }
         else
         {
             NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
             
             // Get championship data
             NSDictionary *count = [dic valueForKey:@"count"];
             NSArray *links = [dic valueForKey:@"_links"];
             NSString *seasonUrl;
             NSString *teamsUrl;
             
             for (NSDictionary* cLink in links) {
                 
                 if ([cLink valueForKey:@"soccerseason"])
                 {
                     seasonUrl = [cLink valueForKey:@"soccerseason"];
                 }
                 else if ([cLink valueForKey:@"self"])
                 {
                     teamsUrl = [cLink valueForKey:@"self"];
                 }
             }
             
             // Save championship to DB
             NSManagedObject *championship = [NSEntityDescription insertNewObjectForEntityForName:@"Championship" inManagedObjectContext:context];
             [championship setValue:count forKey:@"count"];
             [championship setValue:seasonUrl forKey:@"seasonUrl"];
             [championship setValue:teamsUrl forKey:@"teamsUrl"];
             
             NSError *error;
             if (![context save:&error]) {
                 NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
             }
             
             // Get teams data
             NSArray *teams = [dic valueForKey:@"teams"];
             
             for (NSObject *obj in teams)
             {
                 NSDictionary *teamlinks = [obj valueForKey:@"_links"];
                 NSString *fixturesUrl = [[teamlinks valueForKey:@"fixtures"] valueForKey:@"href"];
                 NSString *playersUrl = [[teamlinks valueForKey:@"players"] valueForKey:@"href"];
                 NSString *selfsUrl = [[teamlinks valueForKey:@"self"] valueForKey:@"href"];
                 
                 NSString *code = [obj valueForKey:@"code"];
                 NSString *crestUrl = [obj valueForKey:@"crestUrl"];
                 NSString *name = [obj valueForKey:@"name"];
                 NSString *shortName = [obj valueForKey:@"shortName"];
                 NSString *squadMarketValue = [obj valueForKey:@"squadMarketValue"];
                 
                 // Add team to collection
                 Team *teamEnt = [[Team alloc] init];
                 teamEnt.code = code;
                 teamEnt.crestUrl = crestUrl;
                 teamEnt.name = name;
                 teamEnt.shortName = shortName;
                 teamEnt.squadMarketValue = squadMarketValue;
                 teamEnt.fixturesUrl = fixturesUrl;
                 teamEnt.playersUrl = playersUrl;
                 teamEnt.url = selfsUrl;
                 [[DataManager sharedInstance].teams addObject:teamEnt];
                 [[DataManager sharedInstance].teamCodes addObject:code];
                 
                 // Save team to DB
                 NSManagedObject *team = [NSEntityDescription
                                          insertNewObjectForEntityForName:@"Team"
                                          inManagedObjectContext:context];
                 [team setValue:code forKey:@"code"];
                 [team setValue:crestUrl forKey:@"crestUrl"];
                 [team setValue:name forKey:@"name"];
                 [team setValue:shortName forKey:@"shortName"];
                 [team setValue:squadMarketValue forKey:@"squadMarketValue"];
                 [team setValue:fixturesUrl forKey:@"fixturesUrl"];
                 [team setValue:playersUrl forKey:@"playersUrl"];
                 [team setValue:selfsUrl forKey:@"url"];
                 
                 NSError *error;
                 if (![context save:&error]) {
                     NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                 }
             }
             
             dispatch_async(dispatch_get_main_queue(), ^(void){
                 // Run UI Updates
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"TeamsResolved" object:self];
             });
         }
     }];
}

- (void)requestFixtures:(Team*)team
{
    NSURL *url = [NSURL URLWithString:team.fixturesUrl];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             //NSLog(@"Error,%@", [error localizedDescription]);
         }
         else
         {
             NSDictionary* dic = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
             
             //NSString *count = [dic valueForKey:@"count"];
             
             NSArray *links = [dic valueForKey:@"_links"];
             NSString *fixturesUrl = [[links valueForKey:@"self"] valueForKey:@"href"];
             NSString *teamUrl = [[links valueForKey:@"team"] valueForKey:@"href"];
             
             NSArray *fixtures = [dic valueForKey:@"fixtures"];
             
             NSDictionary *result;
             
             NSMutableArray *fixturesArr = [[NSMutableArray alloc] init];
             Fixture *fix;
             
             for (NSDictionary* fixture in fixtures) {
                 
                 fix = [[Fixture alloc] init];
                 fix.awayTeamName = [fixture valueForKey:@"awayTeamName"];
                 fix.date = [fixture valueForKey:@"date"];
                 fix.homeTeamName = [fixture valueForKey:@"homeTeamName"];
                 fix.matchDay = [[fixture valueForKey:@"matchday"] integerValue];
                 fix.status = [fixture valueForKey:@"status"];
                 fix.url = fixturesUrl;
                 fix.teamUrl = teamUrl;
                 
                 result = [fixture valueForKey:@"result"];
                 fix.away = [result valueForKey:@"goalsAwayTeam"];
                 fix.home = [result valueForKey:@"goalsHomeTeam"];
                 
                 [fixturesArr addObject:fix];
             }
             
             [[DataManager sharedInstance].teamsDic setValue:fixturesArr forKey:team.code];
             
             dispatch_async(dispatch_get_main_queue(), ^(void){
                 // Run UI Updates
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"FixturesResolved" object:self];
             });
         }
     }];
}

@end
