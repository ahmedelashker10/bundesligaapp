//
//  Fixture.h
//  BundesligaApp
//
//  Created by Ahmed Elashker on 6/27/15.
//  Copyright (c) 2015 Ahmed Elashker. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fixture : NSObject

@property (strong, nonatomic) NSString *awayTeamName;

@property (strong, nonatomic) NSString *date;

@property (strong, nonatomic) NSString *homeTeamName;

@property (nonatomic) int matchDay;

@property (strong, nonatomic) NSString *status;

@property (strong, nonatomic) NSString *teamUrl;

@property (strong, nonatomic) NSString *url;

@property (strong, nonatomic) NSNumber *home;

@property (strong, nonatomic) NSNumber *away;

@end
